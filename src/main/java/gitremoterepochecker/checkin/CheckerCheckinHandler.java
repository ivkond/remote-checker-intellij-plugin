package gitremoterepochecker.checkin;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vcs.CheckinProjectPanel;
import com.intellij.openapi.vcs.changes.Change;
import com.intellij.openapi.vcs.checkin.CheckinHandler;
import com.intellij.util.ui.UIUtil;
import git4idea.repo.GitRepository;
import git4idea.repo.GitRepositoryManager;
import gitremoterepochecker.checker.CheckResult;
import gitremoterepochecker.checker.CheckStatus;
import gitremoterepochecker.checker.RepositoryCheckerService;
import gitremoterepochecker.config.CheckerConfig;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

public class CheckerCheckinHandler extends CheckinHandler {
    private CheckinProjectPanel panel;
    private RepositoryCheckerService repositoryCheckerService;

    public CheckerCheckinHandler(CheckinProjectPanel panel, RepositoryCheckerService repositoryCheckerService) {
        this.panel = panel;
        this.repositoryCheckerService = repositoryCheckerService;
    }

    @Override
    public ReturnResult beforeCheckin() {
        if (isCheckEnabled()) {
            return checkRepositories();
        } else {
            return ReturnResult.COMMIT;
        }
    }

    private boolean isCheckEnabled() {
        Project project = panel.getProject();
        PropertiesComponent properties = PropertiesComponent.getInstance(project);
        return CheckerConfig.isCheckBeforeCommit(properties);
    }

    private ReturnResult checkRepositories() {
        Collection<GitRepository> affectedRepositories = getAffectedRepositories();
        if (hasOutdatedRepositories(affectedRepositories)) {
            return askUser();
        } else {
            return ReturnResult.COMMIT;
        }
    }

    private boolean hasOutdatedRepositories(Collection<GitRepository> repositories) {
        for (GitRepository repository : repositories) {
            CheckResult result = repositoryCheckerService.check(repository);
            if (result.getStatus() == CheckStatus.OUTDATED) {
                return true;
            }
        }
        return false;
    }

    private Collection<GitRepository> getAffectedRepositories() {
        GitRepositoryManager repositoryManager = GitRepositoryManager.getInstance(panel.getProject());
        return panel.getSelectedChanges()
                .stream()
                .map(Change::getVirtualFile)
                .filter(Objects::nonNull)
                .map(repositoryManager::getRepositoryForFile)
                .collect(Collectors.toSet());
    }

    private ReturnResult askUser() {
        int decision = Messages.showYesNoDialog(panel.getComponent(),
                "Your local repository is outdated and further action may lead to merge conflicts. It's highly recommended to update project. Commit anyway?",
                "Local Repository Is Outdated",
                UIUtil.getWarningIcon()
        );
        return decision == Messages.YES ? ReturnResult.COMMIT : ReturnResult.CANCEL;
    }
}
