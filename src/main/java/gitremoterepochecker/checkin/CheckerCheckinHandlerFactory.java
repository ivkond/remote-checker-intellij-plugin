package gitremoterepochecker.checkin;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.vcs.CheckinProjectPanel;
import com.intellij.openapi.vcs.changes.CommitContext;
import com.intellij.openapi.vcs.checkin.CheckinHandler;
import com.intellij.openapi.vcs.checkin.CheckinHandlerFactory;
import gitremoterepochecker.checker.RepositoryCheckerService;
import org.jetbrains.annotations.NotNull;

public class CheckerCheckinHandlerFactory extends CheckinHandlerFactory {
    @NotNull
    @Override
    public CheckinHandler createHandler(@NotNull CheckinProjectPanel panel, @NotNull CommitContext commitContext) {
        RepositoryCheckerService repositoryCheckerService = getRepoCheckerService(panel);
        return new CheckerCheckinHandler(panel, repositoryCheckerService);
    }

    private RepositoryCheckerService getRepoCheckerService(@NotNull CheckinProjectPanel panel) {
        return ServiceManager.getService(panel.getProject(), RepositoryCheckerService.class);
    }
}
