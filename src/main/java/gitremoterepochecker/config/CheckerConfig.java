package gitremoterepochecker.config;

import com.intellij.ide.util.PropertiesComponent;
import org.jetbrains.annotations.NotNull;

public class CheckerConfig {
    public static final boolean DEFAULT_CHECK_BEFORE_COMMIT = true;
    public static final boolean DEFAULT_ENABLED = false;
    public static final long DEFAULT_CHECK_PERIOD = 60 * 1000;

    public static final String NAME = "git-remote-repo-checker";
    private static final String PREFIX = NAME + ".";

    private static final String ENABLED = PREFIX + "enabled";
    private static final String CHECK_PERIOD = PREFIX + "check-period";
    private static final String CHECK_BEFORE_COMMIT = PREFIX + "notify-before-commit";

    public static boolean isCheckBeforeCommit(@NotNull PropertiesComponent properties) {
        return properties.getBoolean(CHECK_BEFORE_COMMIT, DEFAULT_CHECK_BEFORE_COMMIT);
    }

    public static void setCheckBeforeCommit(@NotNull PropertiesComponent properties, boolean value) {
        properties.setValue(CHECK_BEFORE_COMMIT, value);
    }

    public static boolean isEnabled(@NotNull PropertiesComponent properties) {
        return properties.getBoolean(ENABLED, DEFAULT_ENABLED);
    }

    public static void setEnabled(@NotNull PropertiesComponent properties, boolean value) {
        properties.setValue(ENABLED, value);
    }

    public static long getPeriod(@NotNull PropertiesComponent properties) {
        return properties.getOrInitLong(CHECK_PERIOD, DEFAULT_CHECK_PERIOD);
    }

    public static void setPeriod(@NotNull PropertiesComponent properties, long value) {
        properties.setValue(CHECK_PERIOD, String.valueOf(value));
    }
}
