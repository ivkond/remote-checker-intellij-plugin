package gitremoterepochecker.auto;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import com.intellij.util.concurrency.AppExecutorUtil;
import git4idea.repo.GitRepository;
import git4idea.repo.GitRepositoryManager;
import gitremoterepochecker.checker.CheckResult;
import gitremoterepochecker.checker.RepositoryCheckerService;
import gitremoterepochecker.config.CheckerConfig;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class AutoCheckerService implements ProjectComponent {
    private static final ScheduledExecutorService EXECUTOR_SERVICE = AppExecutorUtil.getAppScheduledExecutorService();
    private static final String NOTIFICATION_GROUP = "Remote repository checker";
    private static final String UPDATE_PROJECT_ACTION_ID = "Vcs.UpdateProject";

    private ScheduledFuture<?> scheduledFuture;
    private final Project project;
    private final PropertiesComponent properties;

    public AutoCheckerService(Project project) {
        this.project = project;
        this.properties = PropertiesComponent.getInstance(project);
    }

    @Override
    public void projectOpened() {
        initFromConfig();
    }

    @Override
    public void projectClosed() {
        stopAutoFetch();
    }

    public void initFromConfig() {
        if (CheckerConfig.isEnabled(properties)) {
            stopAutoFetch();
            startAutoFetch();
        } else {
            stopAutoFetch();
        }
    }

    private void startAutoFetch() {
        long period = CheckerConfig.getPeriod(properties);
        scheduledFuture = EXECUTOR_SERVICE.scheduleWithFixedDelay(() -> performCheck(project), 0, period, TimeUnit.MILLISECONDS);
    }

    public void stopAutoFetch() {
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
        }
    }

    private void performCheck(Project project) {
        RepositoryCheckerService service = new RepositoryCheckerService();
        List<GitRepository> repositories = GitRepositoryManager.getInstance(project).getRepositories();
        for (GitRepository repository : repositories) {
            checkRepository(service, repository);
        }
    }

    private void checkRepository(RepositoryCheckerService service, GitRepository repository) {
        CheckResult result = service.check(repository);
        switch (result.getStatus()) {
            case OUTDATED:
                notifyRepositoryOutdated();
                break;
            case UNKNOWN:
                notifyRepositoryStatusUnknown(result.getMessage());
                break;
        }
    }

    private void notifyRepositoryOutdated() {
        AnAction updateProjectAction = ActionManager.getInstance().getAction(UPDATE_PROJECT_ACTION_ID);
        Notification notification = new Notification(
                NOTIFICATION_GROUP,
                "Your local repository is outdated",
                "It seems someone have commit new changes in remote repository. It's recommended to update your local copy.",
                NotificationType.WARNING
        );
        notification.addAction(updateProjectAction);

        Notifications.Bus.notify(notification, project);
    }

    private void notifyRepositoryStatusUnknown(String message) {
        Notification notification = new Notification(NOTIFICATION_GROUP, "Remote repository check failed", message, NotificationType.ERROR);
        Notifications.Bus.notify(notification, project);
    }
}
