package gitremoterepochecker.checker;

import com.intellij.openapi.vcs.VcsException;
import git4idea.GitLocalBranch;
import git4idea.GitRemoteBranch;
import git4idea.commands.*;
import git4idea.repo.GitRemote;
import git4idea.repo.GitRepository;

import java.util.List;
import java.util.regex.Pattern;

public class RepositoryCheckerService {
    private static final String DRY_RUN = "--dry-run";

    public CheckResult check(GitRepository repository) {
        try {
            return fetchRepository(repository);
        } catch (VcsException e) {
            return CheckResult.unknown(e.getMessage());
        }
    }

    private CheckResult fetchRepository(GitRepository repository) throws VcsException {
        GitLocalBranch currentBranch = repository.getCurrentBranch();

        if (currentBranch == null) {
            throw new VcsException("No selected branch for repository " + repository.getRoot().toString());
        }

        GitRemoteBranch remoteBranch = currentBranch.findTrackedBranch(repository);

        if (remoteBranch == null) {
            throw new VcsException("No remote configured for branch " + currentBranch.getName());
        }

        return fetchBranch(repository, currentBranch.getName());
    }

    private CheckResult fetchBranch(GitRepository repository, String branchName) {
        GitAuthenticationGate authGate = new GitRestrictingAuthenticationGate();
        GitLineHandler handler = new GitLineHandler(repository.getProject(), repository.getRoot(), GitCommand.FETCH);
        handler.addParameters(DRY_RUN, GitRemote.ORIGIN, branchName);
        handler.setAuthenticationGate(authGate);
        handler.setSilent(true);

        GitCommandResult commandResult = Git.getInstance().runCommand(handler);
        return detectChanges(commandResult);
    }

    private CheckResult detectChanges(GitCommandResult commandResult) {
        if (commandResult.success()) {
            return processCommand(commandResult);
        } else {
            return new CheckResult(CheckStatus.UNKNOWN, commandResult.getErrorOutputAsHtmlString());
        }
    }

    private CheckResult processCommand(GitCommandResult commandResult) {
        List<String> output = getOutput(commandResult);
        if (hasChanges(output)) {
            return new CheckResult(CheckStatus.OUTDATED, "");
        } else {
            return new CheckResult(CheckStatus.UP_TO_DATE, "");
        }
    }

    private List<String> getOutput(GitCommandResult commandResult) {
        return !commandResult.getOutput().isEmpty()
                ? commandResult.getOutput()
                : commandResult.getErrorOutput();
    }

    private boolean hasChanges(List<String> output) {
        Pattern pattern = Pattern.compile("^remote: Total [\\d]+ \\(delta [\\d]+\\).*");
        return output.stream().anyMatch(s -> pattern.matcher(s).matches());
    }
}
