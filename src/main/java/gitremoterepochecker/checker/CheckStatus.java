package gitremoterepochecker.checker;

public enum CheckStatus {
    UNKNOWN,
    UP_TO_DATE,
    OUTDATED
}
