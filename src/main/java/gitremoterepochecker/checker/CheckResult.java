package gitremoterepochecker.checker;

public class CheckResult {
    private CheckStatus status;
    private String message;

    public CheckResult(CheckStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public CheckStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public static CheckResult unknown(String message) {
        return new CheckResult(CheckStatus.UNKNOWN, message);
    }
}
