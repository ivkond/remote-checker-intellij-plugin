package gitremoterepochecker.ui;

public enum CheckPeriod {
    VERY_FREQUENTLY("Very frequently (every 30 second)", 30 * 1000L),
    FREQUENTLY("Frequently (once per minute)", 60 * 1000L),
    OFTEN("Often (every 5 minutes)", 5 * 60 * 1000L),
    SOMETIMES("Sometimes (every 10 minutes)", 10 * 60 * 1000L),
    OCCASIONALLY("Occasionally (every 30 minutes)", 30 * 60 * 1000L),
    SELDOM("Seldom (once per hour)", 60 * 60 * 1000L);

    private String displayValue;
    private long period;

    CheckPeriod(String displayValue, long period) {
        this.displayValue = displayValue;
        this.period = period;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public long getPeriod() {
        return period;
    }

    @Override
    public String toString() {
        return getDisplayValue();
    }
}
