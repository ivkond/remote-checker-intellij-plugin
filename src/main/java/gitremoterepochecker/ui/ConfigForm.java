package gitremoterepochecker.ui;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.project.Project;
import gitremoterepochecker.auto.AutoCheckerService;
import org.jdesktop.swingx.combobox.EnumComboBoxModel;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

import static gitremoterepochecker.config.CheckerConfig.*;

public class ConfigForm implements Configurable {
    private JPanel content;
    private JCheckBox enabledCheckBox;
    private JCheckBox checkBeforeCommitCheckBox;
    private JComboBox<CheckPeriod> periodComboBox;
    private EnumComboBoxModel<CheckPeriod> comboBoxModel;

    private final Project project;
    private final PropertiesComponent properties;

    public ConfigForm(Project project, PropertiesComponent properties) {
        this.project = project;
        this.properties = properties;
        this.comboBoxModel = new EnumComboBoxModel<>(CheckPeriod.class);
    }

    @Nls(capitalization = Nls.Capitalization.Title)
    @Override
    public String getDisplayName() {
        // TODO: move to resource bundle || constants
        return "Git Remote Autochecker";
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        enabledCheckBox.addChangeListener(e -> onEnabledChanged());
        //noinspection unchecked Safe
        periodComboBox.setModel(comboBoxModel);

        initFromConfiguration();

        return content;
    }

    @Override
    public boolean isModified() {
        return isCheckBeforeCommit(properties) != checkBeforeCommitCheckBox.isSelected()
                || isEnabled(properties) != enabledCheckBox.isSelected()
                || getPeriod(properties) != getCheckPeriod();
    }

    @Override
    public void apply() {
        setCheckBeforeCommit(properties, checkBeforeCommitCheckBox.isSelected());
        setEnabled(properties, enabledCheckBox.isSelected());
        setPeriod(properties, getCheckPeriod());

        updateAutoCheckerConfig();
    }

    @Override
    public void reset() {
        initFromConfiguration();
    }

    private void initFromConfiguration() {
        checkBeforeCommitCheckBox.setSelected(isCheckBeforeCommit(properties));
        enabledCheckBox.setSelected(isEnabled(properties));
        periodComboBox.setSelectedItem(findPeriod(getPeriod(properties)));

        onEnabledChanged();
    }

    private void onEnabledChanged() {
        boolean pluginEnabled = enabledCheckBox.isSelected();

        periodComboBox.setEnabled(pluginEnabled);
    }

    private long getCheckPeriod() {
        return comboBoxModel.getSelectedItem().getPeriod();
    }

    private CheckPeriod findPeriod(long value) {
        for (CheckPeriod period : CheckPeriod.values()) {
            if (period.getPeriod() == value) {
                return period;
            }
        }
        throw new IllegalStateException("Unknown period of " + value + "ms");
    }

    private void updateAutoCheckerConfig() {
        AutoCheckerService autoCheckerService = ServiceManager.getService(project, AutoCheckerService.class);
        autoCheckerService.initFromConfig();
    }
}
